<?php
class DataBase {
    private $host = DB_HOST;
    private $username = DB_USER;
    private $password  = DB_PASSWORD;
    private $db_name = DB_NAME;

    private $dbh;
    private $stmt;
    private $error;

    public function __construct() {
        $dsn = 'mysql:host='. $this->host . ';dbname=' . $this-> db_name;
        $options = [PDO::ATTR_PERSISTENT => true,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

        // Instanciamos PDO
        try{
            $this->dbh = new PDO($dsn, $this->username, $this->password, $options);
            $this->dbh->exec('SET NAMES UTF8');
        } catch (PDOException $ex) {
            $this->error = $ex->getMessage();
            echo "Error: ".$this->error;
        }
    }

    public function query($statement) {
        $this->stmt= $this->dbh->prepare($statement);
        if (!$this->stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($this->dbh->errorInfo());
            die("-- --");
        }
    }

    // Vlinculacion
    public function bind($value,$type = null) {
        if(is_null($type)){
            switch ($type) {
                case is_int($value):
                        $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                        $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                        $type = PDO::PARAM_NULL;
                    break;

                default:
                        $type = PDO::PARAM_STR;
                    break;
            }
        }

    }

    //Ejecucion de conslta
    public function execute() {
        try{
            return $this->stmt->execute();
        } catch (PDOException $ex) {
            echo "<strong>Error :</strong> ".$ex->getMessage()."<br><strong>QUERY :</strong> ".$this->stmt->queryString;
            die("<br> ------------------------ FIN ------------------------");
        }

    }

    //Obtener un registro
    public function row() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    //obtener registros
    public function rows() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    //Numero de registros
    public function countRows() {
        $this->stmt->execute();
        return $this->stmt->rowCount();
    }

    //Obtener el nombre del primary key de una tabla
    public function getPrimaryKeyName($tabla) {
        $this->query("SHOW KEYS FROM $tabla WHERE Key_name = 'PRIMARY' ");
        $keys = $this->row();
        return $tabla.".".$keys->Column_name;
    }


    //Insert
    public function insert($table,$param) {
        $campos = "";
        $valores = "";
        foreach($param as $campo => $valor){
            $campos .= $campo . ",";
            $column_password = ["password","password_email"];
            if(in_array($campo,$column_password) ){
                $valores .= "'" . $valor. "',";
            }
            else{
                $valores .= $this->dbh->quote($valor). ",";
            }
        }
        $campos = trim($campos, ',');
        $valores = trim($valores, ',');
        $statement = "INSERT INTO " . $table . "(" . $campos . ") VALUES(" . $valores . ")";
        $this->stmt= $this->dbh->prepare($statement);
        /*if($table == 'ad_cliente_recepcion_doc_declaracion_anual_impuesto_detalle'){
            die($statement);
        }*/
        $this->execute();
        $array_response=["status"=>true,"id"=>$this->dbh->lastInsertId()];

        return $array_response;
    }

    //UPDATE
    public function update($table,$param,$where,$id_pk){

        $set_column_value = "";
        foreach($param as $campo => $valor){
            $column_password = ["password","password_email"];
            if(in_array($campo,$column_password) ){
                $set_column_value .= $campo . " = "."'" . $valor. "',";
            }
            else{
                $set_column_value .= $campo . " = ". $this->dbh->quote($valor). ",";
            }
        }
        $set_column_value = trim($set_column_value,",");
        $statement = "UPDATE $table SET $set_column_value $where ";
        //var_dump($statement);
        /*if($table == 'ad_cliente_recepcion_doc_declaracion_anual_impuesto_detalle'){
            die($statement);
        }*/
        $this->stmt= $this->dbh->prepare($statement);
        $response = $this->execute();
        return true;
    }

    public function delete($tabla,$primary_key_name, $primary_key_value) {
        $statement_select = "SELECT * FROM $tabla WHERE $primary_key_name = $primary_key_value";
        $this->query($statement_select);
        $param = $this->row();
        $statement = "DELETE FROM $tabla WHERE $primary_key_name = $primary_key_value";
        $this->stmt= $this->dbh->prepare($statement);
        $this->execute();
        return true;
    }

    public function beginTransaction() {
        $this->dbh->beginTransaction();
        return true;
    }

    public function rollBack() {
        $this->dbh->rollBack();
        return true;
    }
}