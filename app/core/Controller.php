<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type');
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header('Access-Control-Allow-Headers: Authorization, Content-Type');
    header('Access-Control-Max-Age: 86400');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    exit;
}
class Controller
{

    public $code = 500;
    public $body = ["message" => "Internal Server Error .i."];


    //Carga de modelo
    public function model($model)
    {
        require_once '../app/models/' . ucwords($model) . '.php';
        //Intancia del modelo
        return new $model();
    }


    public function response()
    {
        if ($this->code) {
            http_response_code($this->code);
        }
        echo json_encode($this->body, JSON_PRETTY_PRINT);
        exit;
    }

    public function validateAPI()
    {
        $headers = apache_request_headers();
        $api_key = $headers['Authorization'];
        if ($api_key != AUTH) {
            $this->code = 401;
            $this->body = ["message" => "API Key Incorrecta $api_key"];
            $this->response();
        }
    }
}