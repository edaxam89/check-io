<?php

class Vinculation extends Controller{
    public function __construct(){
        $this->validateAPI();
        $this->vinculationModel = $this->model('vinculationModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function all($param){
        $this->code = 200;
        $this->body = $this->vinculationModel->getAll($param[0]);
        $this->response();
    }

    
    public function allNoti($param){
        $this->code = 200;
        $this->body = $this->vinculationModel->getAllNoti($param[0]);
        $this->response();
    }
    public function name($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->vinculationModel->getByName($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function id($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->vinculationModel->getById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function delete($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->vinculationModel->deleteById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function update($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $umpdate_response = $this->vinculationModel->updateById($param[0],$this->request);
        if($umpdate_response != false){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();
    }



    public function post(){
        $this->code = 400;
        $this->body = ["message" => "Error Insert"];
        $newUser = $this->vinculationModel->post($this->request);
        if($newUser["status"]){
            $this->code = 201;
            $this->body = $newUser;
        }
        $this->response();
    }

    public function recovery(){
        $this->code = 400;
        $this->body = ["message" => "Revisa que el correo este bien escrito"];
        $successUser = $this->vinculationModel->exist($this->request);
        if($successUser != false){
            $to = ["email" => $successUser->email, "name" => $successUser->name ];
            $subject = "Rcuperación de contraseña";
            require_once('tools/SendMail.php');
            $otp_code = $this->vinculationModel->generateOTP($successUser->id_vinculation);
            $html_view = "OTP code : $otp_code";
            $sendMail = sendMail($to, $subject, $html_view);
            if($sendMail){
                
                $this->code = 200;
                $this->body = ["message" => "Revisa tu correo, hemos enviado las indicaciones para la recuperación de tu acceso"];
            } else {
                $this->code = 404;
                $this->body = ["message" => "Intentalo de nuevo"];
            }
        }
        $this->response();
    }

    public function login (){
        $this->code = 401;
        $this->body = ["message" => "Verifica tu correo y contraseña"];
        $successUser = $this->vinculationModel->login($this->request);
        if($successUser != false){
            $timestamp = time();
            $token = sha1($timestamp."-$successUser->id_vinculation");
            $this->vinculationModel->star_session($successUser->id_vinculation, $token);
            $this->code = 200;
            $this->body = ["status" => true,"token" => $token ];
        }
        $this->response();
    }

    public function recoveryOtp(){
        $this->code = 400;
        $this->body = ["message" => "revis tu codigo otp"];
        $successUser = $this->vinculationModel->otp($this->request);
        if($successUser != false){
            $this->code = 200;
            $this->body = ["continue" => true];
        }
        
        $this->response();


    }

    public function recoveryPassword(){
        $this->code = 401;
        $this->body = ["message" => "Verifica tu contraseña"];
        $successUser = $this->vinculationModel->getUserByOtp($this->request);
        if($successUser != false){
            $this->vinculationModel->updatePassword($successUser->id_vinculation,$this->request->password);
            $this->code = 200;
            $this->body = ["status" => true];
        
        }
        $this->response();


    }
    public function token($param){
        $this->code = 404;
        $this->body = ["status" => false];
        if($param[0] != null) { 
            $userNameData = $this->vinculationModel->getByToken($param[0]);
            if($userNameData != false){
                $this->code = 200;
                $this->body = $userNameData;
            } 
        }
        $this->response();
    }

    public function checker(){
        $this->code = 404;
        $this->body = ["status" => false];
        $userNameData = $this->vinculationModel->getByIdVinculation($this->request);
        if($userNameData != false){
            $this->vinculationModel->postHistory($userNameData->id_vinculation,$this->request);
            $this->code = 200;
            $this->body = ["status" => true];
        } 
        $this->response();
    
    }

    public function loginGoogle (){
        $this->code = 401;
        $successUser = $this->vinculationModel->loginGoogle($this->request);
        if($successUser != false){
            $this->vinculationModel->sesion_Google($successUser->id_vinculation,$this->request->token_google);  
            $this->code = 200;
            $this->body = ["status" => true];
        }
        $this->response();
    }

}