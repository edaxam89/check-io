<?php
require_once __DIR__ . '/../../vendor/autoload.php';


class Notification extends Controller {
    public function __construct() {
        $this->validateAPI();
        $this->notificationModel = $this->model('NotificationModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function all($param) {
        $this->code = 200;
        $this->body = $this->notificationModel->getAll($param[0]);
        $this->response();
    }

    public function name($param) {
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->notificationModel->getByName($param[0]);
        if ($userNameData != false) {
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function id($param) {
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->notificationModel->getById($param[0]);
        if ($userNameData != false) {
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function delete($param) {
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->notificationModel->deleteById($param[0]);
        if ($userNameData != false) {
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function update($param) {
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $update_response = $this->notificationModel->updateById($param[0], $this->request);
        if ($update_response != false) {
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();
    }

    public function new($param) {
        $this->code = 400;
        $this->body = ["message" => "Error Insert"];
        $newUser = $this->notificationModel->post($this->request, $param[0]);

        if ($newUser["status"]) {
            $this->code = 201;
            $this->body = $newUser;

            // Enviar notificación
            $title = $this->request->title;
            $message = $this->request->message;
            $topic = "workspace-".$this->request->id_workspace; // Ajusta esto según tus necesidades

            $response = $this->sendNotification($title, $message, $topic);
            $this->body['notification_response'] = json_decode($response, true);
        }

        $this->response();
    }

    function getAccessToken() {
        $client = new Google_Client();
        $client->setAuthConfig(__DIR__ . '/../../credentiales/check-io-enoclabs-firebase-adminsdk-nfeln-eca50e0387.json');
        $client->addScope('https://www.googleapis.com/auth/firebase.messaging');
        $accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
        return $accessToken;
    }

    private function sendNotification($title, $message, $topic) {
        $url = 'https://fcm.googleapis.com/v1/projects/880160051268/messages:send';
        
        $notification = [
            'title' => $title,
            'body' => $message,
            //'sound' => 'default'
        ];

        $data = [
            'message' => [
                'topic' => $topic,
                'notification' => $notification
            ]
        ];

        $accessToken = $this->getAccessToken();
        $headers = [
            "Authorization: Bearer " . $accessToken,
            "Content-Type: application/json; charset=UTF-8"
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);

        if ($response === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }
 
}
?>
