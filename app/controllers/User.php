<?php

class User extends Controller{
    public function __construct(){
        $this->validateAPI();
        $this->userModel = $this->model('UserModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function all(){
        $this->code = 200;
        $this->body = $this->userModel->getAll();
        $this->response();
    }

    public function name($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->userModel->getByName($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function id($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->userModel->getById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function post(){
        $this->code = 400;
        $this->body = ["message" => "Error Insert"];
        $newUser = $this->userModel->post($this->request);
        if($newUser["status"]){
            $this->code = 201;
            $this->body = $newUser;
        }
        $this->response();
    }
}