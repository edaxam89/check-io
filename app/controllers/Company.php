<?php

class Company extends Controller{
    public function __construct(){
        $this->validateAPI();
        $this->companyModel = $this->model('companyModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function all(){
        $this->code = 200;
        $this->body = $this->companyModel->getAll();
        $this->response();
    }

    public function name($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->companyModel->getByName($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function id($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->companyModel->getById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function token($param){
        $this->code = 404;
        $this->body = ["status" => false];
        $userNameData = $this->companyModel->getByToken($param[0]);
        if($userNameData != false){
            $this->code = 200;
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function delete($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->companyModel->deleteById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function update($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $update_response = $this->companyModel->updateById($param[0],$this->request);
        if($update_response != false){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();
    }



    public function post(){
        $this->code = 400;
        $this->body = ["message" => "revisa tu contraseña o el correo ya existe"];
        $password_succes = $this->request->password == $this->request->confirm_password;
        $user_exist = $this->companyModel->exist($this->request) == false;
        if($password_succes && $user_exist ){
            $newUser = $this->companyModel->post($this->request);
            if($newUser["status"]){
                $successUser = $this->companyModel->login($this->request);
                $timestamp = time();
                $token = sha1($timestamp."-$successUser->id_company");
                $this->companyModel->updatestart_session($successUser->id_company,$this->request->keep_session, $timestamp,  $token);
                $this->code = 201;
                $this->body = ["status" => true,"token" => $token ];
            }
        
         }         
        $this->response();
    }

    public function login (){
        $this->code = 401;
        $this->body = ["message" => "Verifica tu correo y contraseña"];
        $successUser = $this->companyModel->login($this->request);
        $existUser = $this->companyModel->getByColumn("email",$this->request->email);
        if($successUser != false){
            $timestamp = time();
            $token = sha1($timestamp."-$successUser->id_company");
            $this->companyModel->updatestart_session($successUser->id_company,$this->request->keep_session, $timestamp,  $token);
            $this->code = 200;
            $this->body = ["status" => true,"token" => $token ];
            $this->companyModel->updateIntent($successUser->id_company,0);
        }
        elseif($existUser != false){
            $counter = $existUser->intent + 1;
            if ($counter == 10);
            $this->companyModel->updateIntent($existUser->id_company,$counter);  
        }
        $this->response();
    }
    public function loginGoogle (){
        $this->code = 401;
        $this->body = ["message" => "Verifica tu correo y contraseña"];
        $successUser = $this->companyModel->loginGoogle($this->request);
        //$existUser = $this->companyModel->getByColumn("email",$this->request->email);
        if($successUser != false){
            $timestamp = time();
            $this->companyModel->updatestart_session_google($successUser->id_company,$this->request->keep_session, $timestamp, $this->request->tokenGoogle);
            $this->code = 200;
            $this->body = ["status" => true,"token" => $this->request->tokenGoogle ];
           
        }
        $this->response();
    }

    public function exist(){
        $this->code = 200;
        $this->body = ["message" => "ír a formulario"];
        $successUser = $this->companyModel->exist($this->request);
        if($successUser != false){
            $this->code = 401;
            $this->body = ["message" => "el correo ya existe"];
        }
        else{
              $para = $this->request->email;
              $asunto = 'Correo de prueba';
              $mensaje = 'Hola, esto es un correo de prueba enviado desde PHP.';

              // Cabeceras
              $headers = 'From: remitente@example.com' . "\r\n" .
                         'Reply-To: remitente@example.com' . "\r\n" .
                         'X-Mailer: PHP/' . phpversion();

               // Envío del correo
               if (mail($para, $asunto, $mensaje, $headers)) {
               echo 'El correo ha sido enviado correctamente.';
               } else {
               echo 'Error al enviar el correo.';
               }
        }
        $this->response();
    }


    public function requestOTPRecobery(){
        $this->code = 400;
        $this->body = ["message" => "Revisa que el correo este bien escrito", "status" => false];
        $successUser = $this->companyModel->exist($this->request);
        if($successUser != false){
            $to = ["email" => $successUser->email, "name" => "$successUser->name $successUser->last_name"];
            $subject = "Rcuperación de contraseña";
            require_once('tools/SendMail.php');
            $otp_code = $this->companyModel->generateOTP($successUser->id_company);
            $html_view = "OTP code : $otp_code";
            $sendMail = sendMail($to, $subject, $html_view);
            if($sendMail){
                
                $this->code = 200;
                $this->body = ["message" => "Revisa tu correo, hemos enviado las indicaciones para la recuperación de tu acceso"];
            } else {
                $this->code = 404;
                $this->body = ["message" => "Intentalo de nuevo"];
            }
        }
        $this->response();
    }


    public function validateOTPRecobery(){
        $this->code = 400;
        $this->body = ["message" => "OTP incorrecto", "status" => false];
        $successUser = $this->companyModel->existOTP($this->request);
        if($successUser != false){
            $this->code = 200;
            $this->body = ["message" => "OTP encontrado"];            
        }
        $this->response();
    }

    public function updatePasswordRecobery(){
        $this->code = 400;
        $this->body = ["message" => "Ocurrio un error al guardar", "status" => false];
        $successUser = $this->companyModel->existOTP($this->request);
        if($successUser != false){
            $this->companyModel->updatePassword($this->request, $successUser->id_company);
            $this->code = 200;
            $this->body = ["message" => "OTP encontrado"];            
        }
        $this->response();
    }

    public function close($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $update_response = $this->companyModel->cleanSession($param[0]);
        if($update_response == NULL){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();

    }


    public function image($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $update_response = $this->companyModel->updateByImage($param[0],$this->request);
        if($update_response != false){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response(); 
    }
}

    
