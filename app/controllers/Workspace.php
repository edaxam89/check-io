<?php

class Workspace extends Controller{
    public function __construct(){ 
        $this->validateAPI();
        $this->workspaceModel = $this->model('workspaceModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function all($param){
        $this->code = 200;
        $this->body = $this->workspaceModel->getAll($param[0]);
        $this->response();
    }

    public function ID_company($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->workspaceModel->getByID_company($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function id($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->workspaceModel->getById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

    public function delete($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $delate_response = $this->workspaceModel->deleteByIdworkspace($param[0]);
        if( $delate_response != false){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();
    }

    public function post($param){
        $this->code = 400;
        $this->body = ["message" => "Error Insert"];
        $newUser = $this->workspaceModel->post($param[0],$this->request);
        if($newUser["status"]){
            $this->code = 201;
            $this->body = ["status" => true];
        }
        $this->response();
    }

    public function update($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $update_response = $this->workspaceModel->updateById($param[0],$this->request);
        if($update_response != false){
            $this->code = 201;
            $this->body = ["status" => "true"];
        }
        $this->response();
    }

    public function idWorkspace($param){
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $userNameData = $this->workspaceModel->getById($param[0]);
        if($userNameData != false){
            $this->body = $userNameData;
        }
        $this->response();
    }

}