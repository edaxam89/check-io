<?php

class Report extends Controller
{
    public function __construct()
    {
        $this->validateAPI();
        $this->workspaceModel = $this->model('workspaceModel');
        $this->scheduleModel = $this->model('scheduleModel');
        $this->vinculationModel = $this->model('vinculationModel');
        $body = file_get_contents('php://input');
        $this->request = json_decode($body);
    }

    public function history($param) {
        $this->code = 404;
        $this->body = ["message" => "Not found - $param[0]"];
        $id_workspace = $param[0];
        $dayStart = $param[1];
        $dayEnd = $param[2];
        $report = [];
    
        if ($id_workspace != "0") {
            $workspaceInfo = $this->workspaceModel->getById($id_workspace);
            $assistenceData = $this->getAssistenceByWorkspace($workspaceInfo->id_workspace, $dayStart, $dayEnd);
            $inasistenciasCount = $this->countInasistencias($assistenceData);
    
            $report = [
                "sucursal" => $workspaceInfo->sucursal,
                "history" => $assistenceData,
                "inasistencias" => $inasistenciasCount
            ];
        }
    
        if ($workspaceInfo != false) {
            $this->code = 200;
            $this->body = $report;
        } else {
            $this->code = 404;
            $this->body = ["message" => "Workspace not found"];
        }
    
        $this->response();
    }


   private function getAssistenceByWorkspace($id_workspace, $dayStart, $dayEnd) {
    $totalColum = $this->getDaysBetween($dayStart, $dayEnd);
    $assistence = $this->vinculationModel->getAssistenceByWorkspace($id_workspace, $dayStart, $dayEnd);

    $diasEspanol = [
        'Monday'    => 'Lunes',
        'Tuesday'   => 'Martes',
        'Wednesday' => 'Miercoles',
        'Thursday'  => 'Jueves',
        'Friday'    => 'Viernes',
        'Saturday'  => 'Sabado',
        'Sunday'    => 'Domingo'
    ];

    $assistenceUser = [];
    $onlyAsisstence = [];
    $lastName = "";
    $allDays = [];
    $i = 0;

    if (isset($assistence[0]->id_vinculation)) {
        $lastIDEmploye = $assistence[0]->id_vinculation;
        $lastIDSchedule = $assistence[0]->id_schedule;
        $nonWorkingDays = $this->vinculationModel->getNonWorkingDays($lastIDSchedule);
        $nonWorkingDaysMap = array_map(function($day) use ($diasEspanol) {
            return $diasEspanol[$day];
        }, $nonWorkingDays);

        foreach ($assistence as $key => $value) {
            $currentIDEmploye = $value->id_vinculation;
            $currentIDSchedule = $value->id_schedule;

            if ($currentIDEmploye != $lastIDEmploye) {
                foreach ($totalColum as $key => $valueTotalColumn) {
                    list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
                    $newValue = "x";
                    if (isset($onlyAsisstence[$valueTotalColumn])) {
                        $newValue = $onlyAsisstence[$valueTotalColumn];
                    } else if (in_array($dayName, $nonWorkingDaysMap)) {
                        $newValue = "No laboral";
                    }
                    $allDays["$valueTotalColumn"] = $newValue;
                }
                $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
                $i++;
                $onlyAsisstence = [];
                $allDays = [];
                $lastIDEmploye = $currentIDEmploye;
                $lastIDSchedule = $currentIDSchedule;
                $nonWorkingDays = $this->vinculationModel->getNonWorkingDays($currentIDSchedule);
                $nonWorkingDaysMap = array_map(function($day) use ($diasEspanol) {
                    return $diasEspanol[$day];
                }, $nonWorkingDays);
            }

            $lastName = $value->name_employe;
            $day = $value->name_day . " " . $value->mydate;
            $newValue = $value->dayAssistence;
            $onlyAsisstence["$day"] = $newValue;
        }

        foreach ($totalColum as $key => $valueTotalColumn) {
            list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
            $newValue = "x";
            if (isset($onlyAsisstence[$valueTotalColumn])) {
                $newValue = $onlyAsisstence[$valueTotalColumn];
            } else if (in_array($dayName, $nonWorkingDaysMap)) {
                $newValue = "No laboral";
            }
            $allDays["$valueTotalColumn"] = $newValue;
        }
        $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
    }

    return $assistenceUser;
}
    
    private function getDaysBetween($dayStart, $dayEnd)
{
    // Crea objetos DateTime
    $datetime1 = new DateTime($dayStart);
    $datetime2 = new DateTime($dayEnd);
    $datetime2->modify('+1 day'); // Incluye el día final

    // Crea un intervalo de un día
    $interval = new DateInterval('P1D');
    $period = new DatePeriod($datetime1, $interval, $datetime2);

    // Mapeo de nombres de días del inglés al español
    $diasEspanol = [
        'Monday'    => 'Lunes',
        'Tuesday'   => 'Martes',
        'Wednesday' => 'Miercoles',
        'Thursday'  => 'Jueves',
        'Friday'    => 'Viernes',
        'Saturday'  => 'Sabado',
        'Sunday'    => 'Domingo'
    ];

    // Arreglo para almacenar los resultados
    $resultados = [];

    // Recorre cada día en el periodo
    foreach ($period as $fecha) {
        $nombreDiaIngles = $fecha->format('l');  // Obtiene el nombre del día en inglés
        $nombreDia = $diasEspanol[$nombreDiaIngles] ?? 'Desconocido'; // Traduce al español
        $numeroDia = $fecha->format('j'); // Obtiene el número del día sin cero inicial
        $resultados[] = $nombreDia . ' ' . $numeroDia;
    }

    return $resultados;
}

private function countInasistencias($assistenceUser) {
    $inasistenciasCount = [];
    
    foreach ($assistenceUser as $user) {
        $name = $user['Nombre'];
        $inasistenciasCount[$name] = 0; // Inicializar el conteo para este usuario
        
        foreach ($user as $day => $status) {
            if ($day !== 'Nombre' && $status === 'x') {
                $inasistenciasCount[$name]++;
            }
        }
    }
    
    return $inasistenciasCount;
}

public function realTimeAttendance($param) {
    $this->code = 404;
    $this->body = ["message" => "Not found - $param[0]"];
    $id_workspace = $param[0];
    $id_vinculation = $param[1];

    // Obtener el primer y último día del mes actual
    $firstDayOfMonth = date("Y-m-01");
    $lastDayOfMonth = date("Y-m-t");
    $currentDay = date("Y-m-d");

    $report = [];

    if ($id_workspace != "0") {
        $workspaceInfo = $this->workspaceModel->getById($id_workspace);

        $assistenceData = $this->getAssistenceByWorkspaceAndIdVinculation($workspaceInfo->id_workspace, $firstDayOfMonth, $currentDay,$id_vinculation);
        $inasistenciasCount = $this->countInasistencias($assistenceData);

        $report = [
            "sucursal" => $workspaceInfo->sucursal,
            "history" => $assistenceData,
            "inasistencias" => $inasistenciasCount
        ];
    }

    if ($workspaceInfo != false) {
        $this->code = 200;
        $this->body = $report;
    }

    $this->response();
}

private function getAssistenceByWorkspaceAndIdVinculation($id_workspace, $dayStart, $dayEnd, $id_vinculation) {
    $totalColum = $this->getDaysBetween($dayStart, $dayEnd);
    $assistence = $this->vinculationModel->getAssistenceByWorkspaceAndIdVinculation($id_workspace, $dayStart, $dayEnd, $id_vinculation);
    
    // Mapeo de nombres de días del inglés al español
    $diasEspanol = [
        'Monday'    => 'Lunes',
        'Tuesday'   => 'Martes',
        'Wednesday' => 'Miercoles',
        'Thursday'  => 'Jueves',
        'Friday'    => 'Viernes',
        'Saturday'  => 'Sabado',
        'Sunday'    => 'Domingo'
    ];

    $assistenceUser = [];
    $onlyAsisstence = [];
    $lastName = "";
    $allDays = [];
    $i = 0;
    
    if (isset($assistence[0]->id_vinculation)) {
        $lastIDEmploye = $assistence[0]->id_vinculation;
        foreach ($assistence as $key => $value) {
            $currentIDEmploye = $value->id_vinculation;
            $nonWorkingDays = $this->vinculationModel->getNonWorkingDays($value->id_schedule);
            // Convertir días no laborables a español
            $nonWorkingDaysMap = array_map(function($day) use ($diasEspanol) {
                return $diasEspanol[$day];
            }, $nonWorkingDays);
            
            if ($currentIDEmploye != $lastIDEmploye) {
                foreach ($totalColum as $key => $valueTotalColumn) {
                    list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
                    $newValue = "x"; // Valor por defecto para falta
                    if (isset($onlyAsisstence[$valueTotalColumn])) {
                        $newValue = $onlyAsisstence[$valueTotalColumn];
                    } else if (in_array($dayName, $nonWorkingDaysMap)) {
                        $newValue = "No laboral"; // Valor para día no laborable
                    }
                    $allDays = $allDays + ["$valueTotalColumn" => $newValue];
                }
                $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
                $lastIDEmploye = $currentIDEmploye;
                $i++;
                $onlyAsisstence = [];
                $allDays = [];
            }
            
            $lastName = $value->name_employe;
            $day = $value->name_day . " " . $value->mydate;
            $newValue = $value->dayAssistence;
            $onlyAsisstence = $onlyAsisstence + ["$day" => $newValue];
        }
        
        foreach ($totalColum as $key => $valueTotalColumn) {
            list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
            $newValue = "x"; // Valor por defecto para falta
            if (isset($onlyAsisstence[$valueTotalColumn])) {
                $newValue = $onlyAsisstence[$valueTotalColumn];
            } else if (in_array($dayName, $nonWorkingDaysMap)) {
                $newValue = "No laboral"; // Valor para día no laborable
            }
            $allDays = $allDays + ["$valueTotalColumn" => $newValue];
        }
        $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
    }
    
    return $assistenceUser;
}


}
