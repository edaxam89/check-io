<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

function sendMail($to,$subject,$html_view){
    // Configuración de la conexión SMTP
    $mail = new PHPMailer(true);
    try {
        $mail->isSMTP();
        $mail->SMTPDebug  = PHPMAILER_SMTP_DEBUG;
        $mail->Host       = MAIL_HOST;
        $mail->SMTPAuth   = true;
        $mail->Username   = MAIL_USERNAME;
        $mail->Password   = MAIL_PASSWORD;
        $mail->SMTPSecure = 'ssl';
        $mail->Port       = 465;
        $mail->Timeout = 3600; 
        // Configuración del mensaje
        $mail->setFrom('no-reply@checkio.com', 'No responder');
        $mail->addAddress($to["email"], $to["name"]);
        $mail->Subject = $subject;
        $mail->Body    = $html_view;

        $mail->send();
        return true;
    } catch (Exception $e) {
        var_dump($to);
        echo "Error al enviar el correo: {$mail->ErrorInfo}";
        exit;
        return false;
    }
}

?>