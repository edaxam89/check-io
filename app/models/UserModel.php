<?php

class UserModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "user";
    }

    public function getAll(){
        $query = "SELECT * FROM user WHERE 1";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getByName($name){
        $query = "SELECT * FROM user WHERE name = '$name'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getById($id){
        $query = "SELECT * FROM user WHERE id = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function post($user){
        $insert["name"] = $user->name;
        $insert["address"] = $user->address;
        $result = $this->db->insert($this->table,$insert);
        return $result;
    }
}