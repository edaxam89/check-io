<?php

class ScheduleModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "schedules";
    }

    public function post($token, $schedule){
        $insert["id_workspace"] = $schedule->id_workspace;
        $insert["profile_name"] = $schedule->profile_name;
        $insert["hour_input"] = $schedule->hour_input;
        $insert["hour_output"] = $schedule->hour_output;
        $insert["monday"] = $schedule->monday;
        $insert["tuesday"] = $schedule->tuesday;
        $insert["wednesday"] = $schedule->wednesday;
        $insert["thursday"] = $schedule->thursday;
        $insert["friday"] = $schedule->friday;
        $insert["saturday"] = $schedule->saturday;
        $insert["sunday"] = $schedule->sunday;
        $result = $this->db->insert($this->table,$insert);
        return $result;
    }

    public function getAll($token){
        $query = "SELECT schedules.*,workspace.sucursal 
        FROM company 
        RIGHT JOIN workspace USING (id_company) 
        RIGHT JOIN schedules USING (id_workspace) 
        WHERE company.token_sesion='$token'";
        $this->db->query($query);

        return $this->db->rows();
    }

    public function deleteByIdschedule($name){
        $query = "DELETE FROM $this->table WHERE id_schedule = '$name'";
        $this->db->query($query);
        return $this->db->execute();
    }

    public function getById($id){
        $query = "SELECT * FROM `schedules` WHERE id_schedule = $id";
        $this->db->query($query);

        return $this->db->row();
    }
    
    public function updateId($id,$schedule){
        $update["id_workspace"] = $schedule->id_workspace;
        $update["profile_name"] = $schedule->profile_name;
        $update["hour_input"] = $schedule->hour_input;
        $update["hour_output"] = $schedule->hour_output;
        $update["monday"] = $schedule->monday;
        $update["tuesday"] = $schedule->tuesday;
        $update["wednesday"] = $schedule->wednesday;
        $update["thursday"] = $schedule->thursday;
        $update["friday"] = $schedule->friday;
        $update["saturday"] = $schedule->saturday;
        $update["sunday"] = $schedule->sunday;

        $where ="WHERE id_schedule = $id";
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;
    }

    public function getAllByWorkspace($id_workspace){
        $query = "SELECT * 
        FROM schedules 
        WHERE id_workspace='$id_workspace'";
        $this->db->query($query);

        return $this->db->rows();
    }
}

