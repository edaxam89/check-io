<?php

class NotificationModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "notification";
    }
    public function post($notification, $token){
        $insert["id_company"] = $this->getByToken($token)->id_company;
        $insert["id_workspace"] = $notification->id_workspace;
        $insert["title"] = $notification->title;
        $insert["message"] = $notification->message;
        $result = $this->db->insert($this->table,$insert);
        
        return $result;
    }
    public function getByToken($token){
        $query = "SELECT * FROM company WHERE token_sesion = '$token'";
        $this->db->query($query);
       return $this->db->row();

        }
 }
