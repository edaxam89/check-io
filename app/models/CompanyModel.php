<?php

class CompanyModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "company";
    }

    public function getAll(){
        $query = "SELECT * FROM $this->table WHERE 1";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getByName($name){
        $query = "SELECT * FROM $this->table WHERE name = '$name'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getById($id){
        $query = "SELECT * FROM $this->table WHERE id_company = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getByToken($token){
        $query = "SELECT * FROM $this->table WHERE token_sesion = '$token'";
        $this->db->query($query);
        return $this->db->row();
    }


    public function deleteById($id){
        $query = "DELETE FROM $this->table WHERE id_company = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }
    
    public function updateById($token,$company){
        $update["email"] = $company->email;
        $update["name"] = $company->name;
        $update["last_name"] = $company->last_name;
        $update["phone"] = $company->phone;
        $update["company"] = $company->company;

       

        $where = "WHERE id_company = ".$this->getByToken($token)->id_company;
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;

    }

    public function post($company){
        $insert["email"] = $company->email;
        $insert["name"] = $company->name;
        $insert["last_name"] = $company->last_name;
        $insert["phone"] = $company->phone;
        $insert["password"] = sha1($company->password);
        $insert["company"] = $company->company;
        $insert["keep_session"] = $company->keep_session;
        $result = $this->db->insert($this->table,$insert);
        return $result;
    }

    public function login($company){
        $sha1_password = sha1($company->password);
        $query = "SELECT id_company, email, name, last_name, phone, company, intent FROM $this->table WHERE email = '$company->email' AND password = '$sha1_password'";
        $this->db->query($query);
        return $this->db->row();
    }
    public function loginGoogle($company){
        $query = "SELECT id_company, email, name, last_name, phone, company, intent FROM $this->table WHERE email = '$company->email'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function exist($company){
        $query = "SELECT * FROM $this->table WHERE email = '$company->email'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function updateIntent($id_company,$intent){
        $update["intent"] = $intent;

        $where = "WHERE id_company = $id_company";
        $result = $this->db->update($this->table,$update,$where,0);
    }

    public function getByColumn($column,$value){
        $query = "SELECT * FROM $this->table WHERE $column = '$value'";
        $this->db->query($query);
        return $this->db->row();
    }


    public function generateOTP($id_company){
        mt_srand (time());
        $otp = mt_rand(0,999999);
        $update["otp"] = $otp;
        $where = "WHERE id_company = $id_company";
        $result = $this->db->update($this->table,$update,$where,0);
        return $otp;
    } 

    public function existOTP($company){
        $query = "SELECT * FROM $this->table WHERE email = '$company->email' AND  otp =  '$company->otp'";
        $this->db->query($query);
        return $this->db->row();
    }

    
    public function updatePassword($company, $id_company){
        $update["password"] = sha1($company->password);
        $where = "WHERE id_company = $id_company";
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;
    }

    public function updatestart_session($id_company, $keep_session, $date_start_session, $token_sesion){
        $update["keep_session"] = $keep_session;
        $update["date_start_session"] = $date_start_session;
        $update["token_sesion"] = $token_sesion;

        $where = "WHERE id_company = $id_company";
        $result = $this->db->update($this->table,$update,$where,0);
    }
    public function updatestart_session_google($id_company, $keep_session, $date_start_session, $tokenGoogle){
        $update["keep_session"] = $keep_session;
        $update["date_start_session"] = $date_start_session;
        $update["token_sesion"] = $tokenGoogle;

        $where = "WHERE id_company = $id_company";
        $result = $this->db->update($this->table,$update,$where,0);
    }
    public function cleanSession($token){
        $update["token_sesion"] = "";

        $where = "WHERE  token_sesion= '$token'";
        $result = $this->db->update($this->table,$update,$where,0);
    }

    public function updateByImage($token,$company){
        $update["logo_company"] = $company->logo_company;
    
        $where = "WHERE id_company = ".$this->getByToken($token)->id_company;
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;

    }
    
}
