<?php

class WorkspaceModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "workspace";
    }

    public function getAll($token){
        $query = "SELECT workspace.* FROM company RIGHT JOIN workspace USING (id_company) WHERE company.token_sesion='$token' AND workspace.status=1";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getByName($name){
        $query = "SELECT * FROM $this->table WHERE name = '$name'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getById($id){
        $query = "SELECT * FROM $this->table WHERE id_workspace = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getByID_company($name){
        $query = "SELECT * FROM $this->table WHERE ID_company = '$name'";
        $this->db->query($query);
        return $this->db->row();
    }
    public function deleteByIdworkspace($name){
        $query = "DELETE FROM $this->table WHERE ID_workspace = '$name'";
        $this->db->query($query);
        return $this->db->execute();
    }

    public function post($token, $workspace){
        $insert["id_company"] = $this->getByToken($token)->id_company;
        $insert["address"] = $workspace->address;
        $insert["range_workspace"] = $workspace->range_workspace;
        $insert["latitude"] = $workspace->latitude;
        $insert["longitude"] = $workspace->longitude;
        $insert["supervisor"] = $workspace->supervisor;
        $insert["sucursal"] = $workspace->sucursal;
        $result = $this->db->insert($this->table,$insert);
        return $result;
    }

    public function updateById($id,$workspace){
        //$update["ID_company"] = $workspace->ID_company;
        $update["address"] = $workspace->address;
        $update["range_workspace"] = $workspace->range_workspace;
        $update["latitude"] = $workspace->latitude;
        $update["longitude"] = $workspace->longitude;
        //$update["status"] = $workspace->status;
        $update["supervisor"] = $workspace->supervisor;
        $update["sucursal"] = $workspace->sucursal;
        

        $where ="WHERE ID_workspace = $id";
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;
    }

    public function getByToken($token){
        $query = "SELECT * FROM company WHERE token_sesion = '$token'";
        $this->db->query($query);
        return $this->db->row();
    }

}
