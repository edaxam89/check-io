<?php

class vinculationModel {
    public function __construct(){
        $this->db = new DataBase;
        $this->table = "vinculation";
        $this->table_history = "employee_work_history";
    }

    public function getAll($token){
        $query = "SELECT vinculation.*, schedules.profile_name FROM company RIGHT JOIN workspace USING (id_company) RIGHT JOIN schedules USING (id_workspace) RIGHT JOIN vinculation USING (id_schedule) WHERE company.token_sesion='$token';";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getAllNoti($token){
        $id_company = $this->getCompanyByToken($token)->id_company;
        $query = "SELECT id_notification, title, message FROM notification WHERE id_company = $id_company ORDER BY `id_notification` DESC;";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getCompanyByToken($token){
        $query = "SELECT * FROM company WHERE token_sesion = '$token'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getByName($name){
        $query = "SELECT * FROM $this->table WHERE name = '$name'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getById($id){
        $query = "SELECT * FROM $this->table WHERE id_vinculation = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }

   
    public function deleteById($id){
        $query = "DELETE FROM $this->table WHERE id_vinculation = '$id'";
        $this->db->query($query);
        return $this->db->row();
    }
    
    public function updateById($id,$vinculation){
        // update($table,$param,$where,$id_pk){
        $update["email"] = $vinculation->email;
        $update["name"] = $vinculation->name;
        $update["last_name"] = $vinculation->last_name;
        $update["id_schedule"] = $vinculation->id_schedule;


        $where = "WHERE id_vinculation = $id";
        $result = $this->db->update($this->table,$update,$where,0);
        return $result;

    }

    public function post($vinculation){
        $insert["id_schedule"] = $vinculation->id_schedule;
        $insert["email"] = $vinculation->email;
        $insert["password"] = $vinculation->password;
        $insert["name"] = $vinculation->name;
        $insert["last_name"] = $vinculation->last_name;
        $result = $this->db->insert($this->table,$insert);
        
        return $result;
    }

    public function exist($vinculation){
        $query = "SELECT * FROM $this->table WHERE email = '$vinculation->email'";
        $this->db->query($query);
        return $this->db->row();
    }
    public function generateOTP($id_vinculation){
        mt_srand (time());
        $otp = mt_rand(0,999999);
        $update["otp"] = $otp;
        $where = "WHERE id_vinculation = $id_vinculation";
        $result = $this->db->update($this->table,$update,$where,0);
        return $otp;
    
    }

    
    public function login($vinculation){
        $query = "SELECT * FROM $this->table WHERE email = '$vinculation->email' AND password='$vinculation->password'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function loginGoogle($vinculation){
        $query = "SELECT * FROM $this->table WHERE email = '$vinculation->email'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function sesion_Google($id_vinculation,$token_google){
        $update["token_Google"] = $token_google;
        $update["token_user"] = $token_google;
        $where = "WHERE id_vinculation = $id_vinculation";
        $result = $this->db->update($this->table,$update,$where,0);
    }

    public function getByColumn($column,$value){
        $query = "SELECT * FROM $this->table WHERE $column = '$value'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function star_session($id_vinculation, $token){
        $update["token_user"] = $token;
        $where = "WHERE id_vinculation = $id_vinculation";
        $result = $this->db->update($this->table,$update,$where,0);
    }

    public function getByToken($token){
        $query = "SELECT * FROM $this->table WHERE token_user = '$token'";
        $this->db->query($query);
        $userInfo = $this->db->row();

        if($userInfo == false){
         return false;   
        }

        $scheduleUser = $userInfo->id_schedule;    
        $scheduleQuery = "SELECT * FROM schedules WHERE id_schedule = ' $scheduleUser'";   
        $this->db->query($scheduleQuery);
        $scheduleInfo = $this->db->row();

        $workspaceUser = $scheduleInfo->id_workspace;
        $workspaceQuery = "SELECT * FROM workspace WHERE id_workspace = '$workspaceUser'";  
        $this->db->query($workspaceQuery);
        $workspaceInfo = $this->db->row();

        $firstDayOfMonth = date("Y-m-01");
        $lastDayOfMonth = date("Y-m-t");
        $currentDay = date("Y-m-d");

        $assistenceData = $this->getAssistenceByWorkspaceAndIdVinculationProces($workspaceInfo->id_workspace, $firstDayOfMonth, $currentDay,$userInfo->id_vinculation);
        $inasistenciasCount = $this->countInasistencias($assistenceData);

        $user = [
                "name" => $userInfo->name,
                "last_name" => $userInfo->last_name,
                "profile_name" => $scheduleInfo->profile_name,
                "latitude"=>$workspaceInfo->latitude,
                "longitude"=>$workspaceInfo->longitude,
                "sucursal"=>$workspaceInfo->sucursal,
                "id_workspace"=>$workspaceInfo->id_workspace,
                "id_vinculation"=>$userInfo->id_vinculation 
                ];
                $response = [

                    "user" => $user,
            
                    "assists" => $this->getAttendance($userInfo->id_vinculation)->attendance,
            
                    "fouls" => $inasistenciasCount,
            
                    "accuracy" => $workspaceInfo->range_workspace,
            
                    "notification" => $this->getNotification($workspaceUser),
            
                    "logo_company" => $this->getImage($workspaceInfo->id_company)->logo_company
            
                ];
            
                return $response;
    }

    public function otp($vinculation){
        $query = "SELECT * FROM $this->table WHERE email = '$vinculation->email' AND otp='$vinculation->otp'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function getUserByOtp($vinculation){
        //$sha1_password = sha1($vinculation->password);
        $query = "SELECT id_vinculation FROM $this->table WHERE email = '$vinculation->email' AND otp = '$vinculation->otp'";
        $this->db->query($query);
        return $this->db->row();
    }

    public function updatePassword($id_vinculation, $password){
        $update["password"] = $password;

        $where = "WHERE id_vinculation = $id_vinculation";
        $result = $this->db->update($this->table,$update,$where,0);
    }

    public function getByIdVinculation($body){
        $query = "SELECT id_vinculation FROM vinculation WHERE token_user = '$body->token'";
        $this->db->query($query);
        return $this->db->row();
    }
    public function postHistory($id_vinculation,$employee_work_history,$attendance=1){
        $insert["id_vinculation"] = $id_vinculation;
        $insert["latitude_input"] = $employee_work_history->latitude_input;
        $insert["longitude_input"] = $employee_work_history->longitude_input;
        $insert["date_register_input"] = $employee_work_history->date_register_input;
        $insert["attendance"] = $attendance;
        $insert["date_register_output"] = $employee_work_history->date_register_output;
        $insert["latitude_output"] = $employee_work_history->latitude_output;
        $insert["longitude_output"] = $employee_work_history->longitude_output;
        $result = $this->db->insert($this->table_history,$insert);
        
        return $result;
    }

    public function getAttendance($id_vinculation){
        $query = "SELECT COUNT(id_vinculation_history) as attendance FROM `employee_work_history` WHERE id_vinculation = $id_vinculation AND attendance=1";
        $this->db->query($query);
        return $this->db->row();
        
    }

    public function getNotification($id_workspace){
        $query = "SELECT id_notification, title, message FROM notification WHERE id_workspace = $id_workspace ORDER BY `id_notification` DESC;";
        $this->db->query($query);
        return $this->db->rows();
        
    }

    public function getImage($id_company){
        $query = "SELECT logo_company FROM company WHERE id_company = '$id_company';";
        $this->db->query($query);
        return $this->db->row();
        
    }

    public function getAssistenceByWorkspace ($id_workspace,$date_start,$date_end){
        $query = "SELECT 
        CONCAT_WS(' ',vinculation.name, vinculation.last_name) as name_employe,
        vinculation.id_vinculation, 
        CASE DAYNAME(employee_work_history.date_register_output) 
          WHEN 'Monday' THEN 'Lunes' 
          WHEN 'Tuesday' THEN 'Martes' 
          WHEN 'Wednesday' THEN 'Miercoles' 
          WHEN 'Thursday' THEN 'Jueves' 
          WHEN 'Friday' THEN 'Viernes' 
          WHEN 'Saturday' THEN 'Sabado' 
          WHEN 'Sunday' THEN 'Domingo' 
          ELSE 'Error' 
        END AS name_day, 
        DAY(employee_work_history.date_register_output) AS mydate, 
        CONCAT(
          TIME(employee_work_history.date_register_input), 
          ' ', 
          TIME(employee_work_history.date_register_output)
        ) AS dayAssistence,
        schedules.id_schedule
      FROM 
        `employee_work_history` 
        LEFT JOIN vinculation USING (id_vinculation) 
        LEFT JOIN schedules USING (id_schedule) 
      WHERE 
        schedules.id_workspace = $id_workspace
        AND CAST(employee_work_history.date_register_input AS DATE) BETWEEN '$date_start' AND '$date_end'
        AND CAST(employee_work_history.date_register_output AS DATE) BETWEEN '$date_start' AND '$date_end' 
      ORDER BY 
        vinculation.name,
        vinculation.last_name, 
        vinculation.id_vinculation
      ";
        $this->db->query($query);
        return $this->db->rows();
    }

    public function getNonWorkingDays($id_schedule) {
        $query = "SELECT monday, tuesday, wednesday, thursday, friday, saturday, sunday 
                  FROM schedules 
                  WHERE id_schedule = $id_schedule 
                  LIMIT 1";
        $this->db->query($query);
        $result = $this->db->row();
        
        $nonWorkingDays = [];
        if ($result) {
            foreach ($result as $day => $isWorking) {
                if ($isWorking == 0) {
                    $nonWorkingDays[] = ucfirst($day);
                }
            }
        }
        return $nonWorkingDays;
    }

    private function countInasistencias($assistenceUser) {
        $inasistenciasCount = [];
        
        foreach ($assistenceUser as $user) {
            $name = $user['Nombre'];
            $inasistenciasCount[$name] = 0; // Inicializar el conteo para este usuario
            
            foreach ($user as $day => $status) {
                if ($day !== 'Nombre' && $status === 'x') {
                    $inasistenciasCount[$name]++;
                }
            }
        }
        
        return $inasistenciasCount[$name];
    }

    public function getAssistenceByWorkspaceAndIdVinculation ($id_workspace,$date_start,$date_end,$id_vinculation){
        $query = "SELECT 
        CONCAT_WS(' ',vinculation.name, vinculation.last_name) as name_employe,
        vinculation.id_vinculation, 
        CASE DAYNAME(employee_work_history.date_register_output) 
          WHEN 'Monday' THEN 'Lunes' 
          WHEN 'Tuesday' THEN 'Martes' 
          WHEN 'Wednesday' THEN 'Miercoles' 
          WHEN 'Thursday' THEN 'Jueves' 
          WHEN 'Friday' THEN 'Viernes' 
          WHEN 'Saturday' THEN 'Sabado' 
          WHEN 'Sunday' THEN 'Domingo' 
          ELSE 'Error' 
        END AS name_day, 
        DAY(employee_work_history.date_register_output) AS mydate, 
        CONCAT(
          TIME(employee_work_history.date_register_input), 
          ' ', 
          TIME(employee_work_history.date_register_output)
        ) AS dayAssistence,
        schedules.id_schedule
      FROM 
        `employee_work_history` 
        LEFT JOIN vinculation USING (id_vinculation) 
        LEFT JOIN schedules USING (id_schedule) 
      WHERE 
        employee_work_history.id_vinculation = $id_vinculation
        AND schedules.id_workspace = $id_workspace
        AND CAST(employee_work_history.date_register_input AS DATE) BETWEEN '$date_start' AND '$date_end'
        AND CAST(employee_work_history.date_register_output AS DATE) BETWEEN '$date_start' AND '$date_end' 

      ORDER BY 
        vinculation.name,
        vinculation.last_name, 
        vinculation.id_vinculation
      ";
        $this->db->query($query);
        return $this->db->rows();
    }


    private function getAssistenceByWorkspaceAndIdVinculationProces($id_workspace, $dayStart, $dayEnd, $id_vinculation) {
        $totalColum = $this->getDaysBetween($dayStart, $dayEnd);
        $assistence = $this->getAssistenceByWorkspaceAndIdVinculation($id_workspace, $dayStart, $dayEnd, $id_vinculation);
        
        // Mapeo de nombres de días del inglés al español
        $diasEspanol = [
            'Monday'    => 'Lunes',
            'Tuesday'   => 'Martes',
            'Wednesday' => 'Miercoles',
            'Thursday'  => 'Jueves',
            'Friday'    => 'Viernes',
            'Saturday'  => 'Sabado',
            'Sunday'    => 'Domingo'
        ];
    
        $assistenceUser = [];
        $onlyAsisstence = [];
        $lastName = "";
        $allDays = [];
        $i = 0;
        
        if (isset($assistence[0]->id_vinculation)) {
            $lastIDEmploye = $assistence[0]->id_vinculation;
            foreach ($assistence as $key => $value) {
                $currentIDEmploye = $value->id_vinculation;
                $nonWorkingDays = $this->getNonWorkingDays($value->id_schedule);
                // Convertir días no laborables a español
                $nonWorkingDaysMap = array_map(function($day) use ($diasEspanol) {
                    return $diasEspanol[$day];
                }, $nonWorkingDays);
                
                if ($currentIDEmploye != $lastIDEmploye) {
                    foreach ($totalColum as $key => $valueTotalColumn) {
                        list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
                        $newValue = "x"; // Valor por defecto para falta
                        if (isset($onlyAsisstence[$valueTotalColumn])) {
                            $newValue = $onlyAsisstence[$valueTotalColumn];
                        } else if (in_array($dayName, $nonWorkingDaysMap)) {
                            $newValue = "No laboral"; // Valor para día no laborable
                        }
                        $allDays = $allDays + ["$valueTotalColumn" => $newValue];
                    }
                    $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
                    $lastIDEmploye = $currentIDEmploye;
                    $i++;
                    $onlyAsisstence = [];
                    $allDays = [];
                }
                
                $lastName = $value->name_employe;
                $day = $value->name_day . " " . $value->mydate;
                $newValue = $value->dayAssistence;
                $onlyAsisstence = $onlyAsisstence + ["$day" => $newValue];
            }
            
            foreach ($totalColum as $key => $valueTotalColumn) {
                list($dayName, $dayNumber) = explode(' ', $valueTotalColumn);
                $newValue = "x"; // Valor por defecto para falta
                if (isset($onlyAsisstence[$valueTotalColumn])) {
                    $newValue = $onlyAsisstence[$valueTotalColumn];
                } else if (in_array($dayName, $nonWorkingDaysMap)) {
                    $newValue = "No laboral"; // Valor para día no laborable
                }
                $allDays = $allDays + ["$valueTotalColumn" => $newValue];
            }
            $assistenceUser[$i] = ["Nombre" => $lastName] + $allDays;
        }
        
        return $assistenceUser;
    }

    private function getDaysBetween($dayStart, $dayEnd)
    {
        // Crea objetos DateTime
        $datetime1 = new DateTime($dayStart);
        $datetime2 = new DateTime($dayEnd);
        $datetime2->modify('+1 day'); // Incluye el día final
    
        // Crea un intervalo de un día
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($datetime1, $interval, $datetime2);
    
        // Mapeo de nombres de días del inglés al español
        $diasEspanol = [
            'Monday'    => 'Lunes',
            'Tuesday'   => 'Martes',
            'Wednesday' => 'Miercoles',
            'Thursday'  => 'Jueves',
            'Friday'    => 'Viernes',
            'Saturday'  => 'Sabado',
            'Sunday'    => 'Domingo'
        ];
    
        // Arreglo para almacenar los resultados
        $resultados = [];
    
        // Recorre cada día en el periodo
        foreach ($period as $fecha) {
            $nombreDiaIngles = $fecha->format('l');  // Obtiene el nombre del día en inglés
            $nombreDia = $diasEspanol[$nombreDiaIngles] ?? 'Desconocido'; // Traduce al español
            $numeroDia = $fecha->format('j'); // Obtiene el número del día sin cero inicial
            $resultados[] = $nombreDia . ' ' . $numeroDia;
        }
    
        return $resultados;
    }
    
}
    
