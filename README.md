# MIDDLEWARE PHP

### Pre-requisitos 📋
- *PHP* , Se recomienda utilizar la ultima versión de PHP, sin embargo funciona en versiones 5.6 en adelante.

- *MySQL* , Se recomienda utilizar en versiones 5.6.43 o superiores.

_Re-escritura con archivos .htaccess, generalmente ya viene incluido en XAMPP y en los servidores productivos_



## Instalación 🔧

_La instalación es sencilla solo se necesita clonar el repositorio en la carpeta publica del servidor_

#### _1.- Clonar Proyecto_

```
SSH
git clone git@gitlab.com:edaxam89/base-middleware.git

HTTPS
https://gitlab.com/edaxam89/base-middleware.git
```

#### _3.- Base de Datos_
* Definir las constantes
```
define('DB_HOST','X.X.X.X');
define('DB_USER','??????');
define('DB_PASSWORD','???????????');
define('DB_NAME','???????');
```


* Ejemplo de DB
```


-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 20-05-2023 a las 17:43:49
-- Versión del servidor: 5.7.34
-- Versión de PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `user`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


```


## Archivos de Configuración  ⚙️ 🚀


Para poder montar el sistema se necesita crear dosr archivos un _config.php_ y un _.htaccess_.

Ejemplo de de archivos:
- *config.php * , ubicado en app/

```
<?php

/* DATA BASE Config */
define('DB_HOST','localhost:8889');
define('DB_USER','root');
define('DB_PASSWORD','root');
define('DB_NAME','user');

/* AUTH Config */
define('AUTH', '76e082c90d5310f9f3a21915650798e3d413a597ca1553c184d1d4b527a4367e');
```

- *.htaccess* , ubicado en public/
En la linea se pone el nombre de la carpeta donde se ubica el proyecto dentro
de la carpeta publica, si vive en un dominio o sub dominio solo se pone /
para el ejemplo siguiente esta en una carpeta de la carpeta publica
```
<IfModule mod_rewrite.c>
Options -Multiviews
RewriteEngine On
RewriteBase /base-middleware/
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]
</IfModule>
```

